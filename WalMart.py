#coding: utf-8
import requests
import os
import time
import sys
import json

from bs4 import BeautifulSoup
from flask import Flask, request
from slack import WebClient
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from slack.web.classes.interactions import MessageInteractiveEvent
from slackeventsapi import SlackEventAdapter
from selenium import webdriver

SLACK_TOKEN = "xoxb-672078783539-689239972148-ubKZlu76XJ8MQ1Ba0ebNwQDZ"
SLACK_SIGNING_SECRET = "35d39bc520d4c573932ecba9e8c3bf1b"

# dongbot
# SLACK_TOKEN = "xoxb-678398682979-690945613488-eYhoOm10RTAtTgEDxYqZG41I"
# SLACK_SIGNING_SECRET = "1777abd9e669ce3b09b013ea25de50fd"

app = Flask(__name__)

slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

def gs_search(text):
	options = webdriver.ChromeOptions()
	options.add_argument('headless')
	options.add_argument('window-size=1920x1080')
	options.add_argument("disable-gup")
	driver = webdriver.Chrome(os.getcwd() + "\\chromedriver.exe", chrome_options=options)

	# driver = webdriver.Chrome(os.getcwd()+"\\chromedriver.exe")

	driver.get("http://gs25.gsretail.com/gscvs/ko/products/event-goods")
	driver.implicitly_wait(1)

	s = driver.find_element_by_id('searchWord')
	s.send_keys(text)

	#s.submit()

	#driver.find_element_by_xpath("""// *[ @ id = "searchBtn"]""").click()
	
	driver.find_element_by_id('searchBtn').click()
	time.sleep(2)

	driver.find_element_by_id('TOTAL').click()
	time.sleep(2)

	soup = BeautifulSoup(driver.page_source, "html.parser")

	res = []
	img_src = []

	for page in soup.find('ul', class_='prod_list').find_all('div', class_='prod_box'):
		name = page.find('p', class_='tit').get_text()
		price = page.find('p', class_='price').get_text()
		event = page.find('div', class_='flag_box').get_text()
		
		res.append(u"{} *{}* `{}`".format(name, price, event))
		img_src.append(page.find('img')['src'])

	driver.quit()

	if len(res) == 0:
		return (u"행사중인 상품이 없습니다.", u"")

	return (res, img_src)

def cu_search(text):

	options = webdriver.ChromeOptions()
	options.add_argument('headless')
	options.add_argument('window-size=1920x1080')
	options.add_argument("disable-gup")

	driver = webdriver.Chrome(os.getcwd()+"\\chromedriver.exe", chrome_options=options)

	# driver = webdriver.Chrome(os.getcwd()+"\\chromedriver.exe")
	driver.get("http://cu.bgfretail.com/product/search.do")
	driver.implicitly_wait(1)

	s = driver.find_element_by_id('searchKeyword')
	s.send_keys(text)
	s.submit()

	time.sleep(2)

	soup = BeautifulSoup(driver.page_source, "html.parser")
	res = []
	img_src = []

	for page in soup.find("div", class_="prodListWrap").find("ul").find_all("li"):
		if len(page.get_text()) <= 4 or page.get_text() == u"아침애행사":
			continue

		name = page.find("p", class_="prodName").get_text()
		price = page.find("p", class_="prodPrice").get_text()
		event = page.find("li")

		if event:
			res.append(u"{} *{}* `{}`".format(name, price, event.get_text()))
			img_src.append(page.find("div", class_="photo").find("img")["src"])
		else:
			# res.append(u"{} {}".format(name, price))
			continue

	driver.quit()

	if len(res) == 0:
		return (u"행사중인 상품이 없습니다.", u"")

	return (res, img_src)


@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
	channel = event_data["event"]["channel"]
	text = event_data["event"]["text"]
	ts = event_data["event_time"]

	flag = False

	with open("timestampinfo.txt", "r") as f:
		t = str(f.readline())
		if str(t) == str(ts):
			flag = True

	if flag:
		return

	with open("timestampinfo.txt", "w") as f:
		f.write(str(ts))

	try:
		text = text.split()[1]

		cu_messages, cu_img = cu_search(text)
		gs_messages, gs_img = gs_search(text)

		test_section = [
			{
				"type": "context",
				"elements": [
					{
						"type": "image",
						"image_url": "https://image.freepik.com/free-photo/red-drawing-pin_1156-445.jpg",
						"alt_text": "images"
					},
					{
						"type": "mrkdwn",
						"text": "*<http://cu.bgfretail.com/product/index.do?category=product&depth1=1&sf=N\|CU>*"
					}
				]
			}
		]

		if len(cu_img):
			for i in range(len(cu_messages)):
				test_section.append(
					{
						"type": "section",
						"text": {
							"type": "mrkdwn",
							"text": ":point_right: " + cu_messages[i]
							},
						"accessory": {
							"type": "image",
							"image_url": cu_img[i],
							"alt_text": " "
							}
					}
				)
		else:
			test_section.append(
				{
					"type": "section",
					"text": {
						"type": "mrkdwn",
						"text": ":point_right: " + cu_messages
						}
				}
			)

		test_section.append(
			{
				"type": "divider"
			}
		)

		test_section.append(
			{
				"type": "context",
				"elements": [
					{
						"type": "image",
						"image_url": "https://image.freepik.com/free-photo/red-drawing-pin_1156-445.jpg",
						"alt_text": "images"
					},
					{
						"type": "mrkdwn",
						"text": "*<http://gs25.gsretail.com/gscvs/ko/products/event-goods#\|GS25>*"
					}
				]
			}
		)

		if len(gs_img):
			for i in range(len(gs_messages)):
				test_section.append(
					{
						"type": "section",
						"text": {
							"type": "mrkdwn",
							"text": ":point_right: " + gs_messages[i]
							},
						"accessory": {
							"type": "image",
							"image_url": gs_img[i],
							"alt_text": "1"
							}
					}
				)

		else:
			test_section.append(
				{
					"type": "section",
					"text": {
						"type": "mrkdwn",
						"text": ":point_right: " + cu_messages
						}
				}
			)

		slack_web_client.chat_postMessage(
			channel=channel,
			blocks=extract_json(
				test_section
			)
		)


	except Exception as e:
		print("Error: {}".format(e))
		if "NoneType" in str(e):
			slack_web_client.chat_postMessage(
				channel=channel,
				text=u"상품명 또는 상품명의 일부를 입력하세요 :white_frowning_face:"
				)
			return "OK", 200

		slack_web_client.chat_postMessage(
			channel=channel,
			text=u"상품명 또는 상품명의 일부를 입력하세요 :white_frowning_face:"
			)
	

	return "OK", 200

@slack_events_adaptor.on("app_home_opened")
def app_home_opened(event_data):
	print('1234')
	channel = event_data["channel"]
	slack_web_client.chat_postMessage(
		channel=channel,
		text=u":point_right: 사용법 : @dongbot [상품명]\nex) @dongbot 초코에몽\n:grinning:"
		)

	return "OK", 200


@app.route("/", methods=["GET"])
def index():
	return "<h1>Server is ready.</h1>"

@app.route("/hello", methods=["GET"])
def hello():
	payload = request.values["payload"]
	event = MessageInteractiveEvent(json.loads(payload))
	slack_web_client.chat_postMessage(
		channel=event.channel.id,
		text=u":point_right: 사용법 : @dongbot [상품명]\nex) @dongbot 초코에몽\n:grinning:"
		)

	return "OK", 200

if __name__ == '__main__':
	app.run('0.0.0.0', port=8080)


# :grinning:
# :point_right: